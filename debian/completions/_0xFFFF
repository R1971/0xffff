#compdef 0xFFFF

flaglist=(
	'no-omap-wd[disable auto reboot by OMAP watchdog]'
	'no-ext-wd[disable auto reboot by external watchdog]'
	'no-lifeguard-reset[disable auto reboot by software lifeguard]'
	'serial-console[enable serial console]'
	'no-usb-timeout[disable usb timeout for flashing]'
	'sti-console[enable sti console]'
	'no-charging[disable battery charging]'
	'force-power-key[force omap boot reason to power key]'
)

typelist=(
	'xloader'
	'2nd'
	'secondary'
	'kernel'
	'initfs'
	'rootfs'
	'mmc'
	'cmt-2nd'
	'cmt-algo'
	'cmt-mcusw'
)

devlist=(
	'SU-18'
	'RX-34'
	'RX-44'
	'RX-48'
	'RX-51'
)

_dev() {
	_wanted devices expl 'device' \
	compadd -S: "$@" $devlist
}

_hw() {
	# cannot auto-complete hardware
}

_ver() {
	# cannot auto-complete version
}

_type() {
	compset -P "*:"
	compset -P "*,"
	_wanted types expl 'types' \
	compadd -q -S: "$@" $typelist
}

_img() {
	compset -P "*:"
	compset -P "*%"
	_files -q -S%
}

_0xFFFF() {
	local context curcontext="$curcontext" state line state_descr val_args opt_args
	typeset -A flags types

	_arguments -C \
		'-b[boot default or loaded kernel (default: no cmdline)]::' \
		'-r[reboot device]' \
		'-l[load kernel and initfs images to RAM]' \
		'-f[flash all specified images]' \
		'-c[cold flash 2nd and secondary image]' \
		'-x[check for bad blocks on mtd device (default: all)]::' \
		'-E[dump all device images to one fiasco image, see -t]:' \
		'-e[dump all device images to directory, see -t (default: current)]:directory:' \
		'-I[identify, show all information about device]' \
		'-D[change root device]:device:((0\:flash 1\:mmc 2\:usb))' \
		'-U[enable/disable USB host mode]:bool:((0\:disable 1\:enable))' \
		'-R[enable/disable R&D mode]:bool:((0\:disable 1\:enable))' \
		'-F[change R&D flags, flags are comma separated list]::flags:->flags' \
		'-H[change HW revision]:revision' \
		'-N[change NOLO version string]:version' \
		'-K[change kernel version string]:version' \
		'-T[change initfs version string]:version' \
		'-S[change SW release version string]:version' \
		'-C[change content eMMC version string]:version' \
		'-M[specify fiasco image]:file' \
		'*-m[specify normal image]:argument:->image' \
		'-t[filter images by type]:types:->types' \
		'-d[filter images by device]:device' \
		'-w[filter images by HW revision]:hardware' \
		'-u[unpack fiasco image to directory (default: current)]::directory:_files -/' \
		'-g[generate fiasco image with SW rel version (default: no version)]:file:_files' \
		'-i[identify images]' \
		'-s[simulate, do not flash or write on disk]' \
		'-n[disable hash, checksum and image type checking]' \
		'-v[be verbose and noisy]' \
		'-h[show this help message]'

	case "$state" in
		flags)
			_values -s , "R&D flags" ${flaglist}
			;;
		types)
			_values -s , "Types" ${typelist}
			;;
		image)
			#arg is [[[dev:[hw:]]ver:]type:]file[%lay]
			local hasdev=false hastype=false

			PARTS=`echo $PREFIX | grep -o : | wc -l`
			if [[ "$PARTS" != "0" ]] ; then
				for p in `seq $PARTS` ; do
					part=`echo $PREFIX | cut -d : -f $p`
					IFS=" "
					for d in $devlist ; do
						if [[ "$part" == "$d" ]] ; then
							hasdev=true;
						fi
					done
					for t in $typelist ; do
						# comma separated list
						echo "$part" | grep -q "$t"
						if [ $? -eq 0 ] ; then
							hastype=true;
						fi
					done
				done
			fi

			if [[ "$PARTS" == "0" ]] ; then
				_alternative \
					'devices:device:_dev' \
					'version:ver:_ver' \
					'types:type:_type' \
					'files:file:_img'
			elif [[ "$hastype" == "true" ]] ; then
				_alternative \
					'files:file:_img'
			elif [[ "$PARTS" == "1" && "$hasdev" == "true" ]] ; then
				_alternative \
					'hardware:hw:_hw' \
					'version:ver:_ver' \
					'types:type:_type' \
					'files:file:_img'
			elif [[ "$PARTS" == "1" ]] ; then
					_alternative \
					'types:type:_type' \
					'files:file:_img'			
			elif [[ "$PARTS" == "2" && "$hasdev" == "true" ]] ; then
				_alternative \
					'version:ver:_ver' \
					'types:type:_type' \
					'files:file:_img'
			elif [[ "$PARTS" == "3" ]] ; then
				_alternative \
					'types:type:_type' \
					'files:file:_img'
			fi
			;;
	esac

}

_0xFFFF "$@"
